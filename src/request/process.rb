require 'relaxo'

module Request
  class Process
    REPOSITORIES_FOLDER = 'repositories/'

    def initialize(path, verb, params)
      @path = path
      @verb = verb
      @params = params
    end

    attr_reader :params, :verb, :path

    def call
      db = Relaxo.connect(REPOSITORIES_FOLDER + 'log-store')

      cleaned_path = path[1..-1]

      # TODO: Implement other write methods
      if verb == 'post'
        db.commit(message: "Posting #{path}") do |dataset|
          puts 'inserting path: ' + path

          object = dataset.append(params.to_json)
          dataset.write(cleaned_path, object)
        end
      end

      if verb == 'get'
        cleaned_path = path[1..-1]
        db.current[cleaned_path].data
      end
    end
  end
end
