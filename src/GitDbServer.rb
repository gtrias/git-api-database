require 'sinatra'
require "sinatra/base"
require "sinatra/multi_route"
require 'bundler/setup'

require_relative './swagger/reader.rb'
require_relative './request/process.rb'


module GitDB
  class Server < Sinatra::Base
    register Sinatra::MultiRoute

    # set some settings for development
    configure :development do
      set :reload_stuff, true
    end

    definition = SwaggerReader.new('log-store.yml').definition

    get '/swagger.json' do
      content_type :json
      definition.to_json
    end

    # Converting swagger definition to real API
    base_path = definition['basePath']

    definition['paths'].each do |path|
      path_url = base_path + path[0]
      path_definitions = path[1]

      path_definitions.each do |definition|
        verb = definition[0]

        puts 'registering: ' + verb.to_s.upcase + ' - ' + path_url.to_s

        # Dynamic generated route
        route verb.to_sym, path_url.to_s do
          Request::Process.new(
            path_url.to_s,
            verb,
            params
          ).call
        end
      end
    end
  end
end
