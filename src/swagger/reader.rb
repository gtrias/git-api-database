require 'swagger'

class SwaggerReader
  DEFINITIONS_PATH = '../../data/'

  # @return [Json]
  def initialize(file)
    @swagger_path = DEFINITIONS_PATH + file || "/petstore.yml"
  end

  def definition
    load_file
  end

  private

  attr_reader :swagger_path

  def load_file
    Swagger.load(
      File.join(
        File.dirname(__FILE__), swagger_path
      )
    )
  end
end
