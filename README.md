# Git API Database

Create an API from swagger files and persist received data to bare git repository.

## Install dependencies

```bash
bundle install
```

## Start server

```bash
bundle exec rackup -p 3000
```

## Powered by

* [relaxo](https://github.com/ioquatix/relaxo) - Relaxo is a transactional document database built on top of git.
* [sinatra](http://sinatrarb.com/) - Sinatra is a DSL for quickly creating web applications in Ruby with minimal effort:
