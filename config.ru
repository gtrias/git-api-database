require 'rubygems'
require 'bundler'

Bundler.require

require './src/GitDbServer.rb'
run GitDB::Server
