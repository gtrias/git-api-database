FROM ruby:2.4
MAINTAINER genar@acs.li

# Install dependencies
RUN apt-get update -qq && apt-get install -y build-essential cmake
RUN mkdir /myapp
WORKDIR /myapp
ADD . /myapp
RUN gem install bundler -v 2
RUN bundle install
#
# # Publish port 3000
EXPOSE 3000
#
# # Start rails built-in server
ENTRYPOINT bundle exec rackup -p 3000 -b 0.0.0.0
